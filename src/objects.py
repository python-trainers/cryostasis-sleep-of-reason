from trainerbase.gameobject import GameFloat
from trainerbase.memory import pm


player_health = GameFloat(pm.base_address + 0x8381074)

ammo_in_clip_rifle = GameFloat(pm.base_address + 0x83E53DC)
total_ammo_rifle = GameFloat(pm.base_address + 0x83E52C8)

ammo_in_clip_watergun = GameFloat(pm.base_address + 0x83E547C)
total_ammo_watergun = GameFloat(pm.base_address + 0x83E52DC)

ammo_in_clip_ppsh = GameFloat(pm.base_address + 0x83E53F4)
total_ammo_ppsh = GameFloat(pm.base_address + 0x83E52CC)
