from dearpygui import dearpygui as dpg
from trainerbase.gui import GameObjectUI, SeparatorUI, SpeedHackUI, TextUI, add_components, simple_trainerbase_menu

from objects import (
    ammo_in_clip_ppsh,
    ammo_in_clip_rifle,
    ammo_in_clip_watergun,
    player_health,
    total_ammo_ppsh,
    total_ammo_rifle,
    total_ammo_watergun,
)


@simple_trainerbase_menu("Cryostasis Sleep of Reason", 750, 380)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(
                    player_health,
                    "HP",
                    freeze_hotkey="Shift+F1",
                    set_hotkey="F1",
                    default_setter_input_value=200,
                ),
                SeparatorUI(),
                SpeedHackUI(),
            )
        with dpg.tab(label="Ammo"):
            add_components(
                TextUI("Rifle"),
                GameObjectUI(
                    ammo_in_clip_rifle,
                    "In Clip",
                    freeze_hotkey="Shift+F2",
                    set_hotkey="F2",
                    default_setter_input_value=5,
                ),
                GameObjectUI(
                    total_ammo_rifle,
                    "Total",
                    freeze_hotkey="Shift+F3",
                    set_hotkey="F3",
                    default_setter_input_value=20,
                ),
                SeparatorUI(),
                TextUI("Watergun"),
                GameObjectUI(
                    ammo_in_clip_watergun,
                    "In Clip",
                    freeze_hotkey="Shift+F4",
                    set_hotkey="F4",
                    default_setter_input_value=5,
                ),
                GameObjectUI(
                    total_ammo_watergun,
                    "Total",
                    freeze_hotkey="Shift+F6",
                    set_hotkey="F6",
                    default_setter_input_value=20,
                ),
                SeparatorUI(),
                TextUI("PPSh"),
                GameObjectUI(
                    ammo_in_clip_ppsh,
                    "In Clip",
                    freeze_hotkey="Shift+F7",
                    set_hotkey="F7",
                    default_setter_input_value=71,
                ),
                GameObjectUI(
                    total_ammo_ppsh,
                    "Total",
                    freeze_hotkey="Shift+F8",
                    set_hotkey="F8",
                    default_setter_input_value=200,
                ),
            )
